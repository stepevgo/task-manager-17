package ru.t1.stepanishchev.tm.command.task;

import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getTaskService().changeTaskStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public String getName() {
        return "task-start-by-id";
    }

    @Override
    public String getDescription() {
        return "Start task by id.";
    }

}