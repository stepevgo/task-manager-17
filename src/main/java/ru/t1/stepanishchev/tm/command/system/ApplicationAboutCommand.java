package ru.t1.stepanishchev.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Evgeniy Stepanishchev");
        System.out.println("e-mail: estepanischev@t1-consulting.ru");
        System.out.println("e-mail: stepevgo.vrn@gmail.com");
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Show info about developer.";
    }

}